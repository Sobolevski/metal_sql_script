import requests
import datetime
from data_source import DataSource
from metal import Metal
from metal import Price


NBRB_URL_LIST_OF_METALS = 'http://www.nbrb.by/API/Metals'
NBRB_URL_GET_PRICES = 'http://www.nbrb.by/API/BankIngots/Prices/'


class Nbrb(DataSource):
    def get_json(self, url, date=None):
        try:
            if date == None:
                req = requests.get(url)
            else:
                req = requests.get(url, params=date)
                if req.text == '[]':
                    raise Exception('На запрашиваемый период цены не установлены')
            req.raise_for_status()
            return req.json()
        except requests.ConnectionError:
            raise Exception(requests.ConnectionError('Нет соединения с интернетом'))
        except requests.HTTPError as http_error:
            raise Exception(requests.HTTPError(http_error))

    def type_to_id(self, names, metal_type):
        for name in names:
            if metal_type == name['NameEng'] or metal_type == name['Name'] or metal_type == name['NameBel']:
                return name['Id']
        raise ValueError('Неправильное название металла')

    def get_prices(self):
        TODAY = datetime.datetime.now().strftime("%Y-%m-%d")
        names = Nbrb.get_json(self, NBRB_URL_LIST_OF_METALS)
        date = {
            'startDate': TODAY,
            'endDate': TODAY
        }
        url = NBRB_URL_GET_PRICES
        metals = Nbrb.get_json(self, url, date)
        ids = [d['Name'] for d in names]
        result = []
        for ingot in metals:
            metal = Metal()
            metal.name = ids[ingot['MetalId']]
            metal.date = ingot['Date'][:10]
            metal.price = Price(soldBYN=ingot['Value'])
            result.append(metal)
        return result