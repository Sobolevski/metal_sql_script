''' Базы данных:
CREATE DATABASE metal_price DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
mysql> CREATE TABLE price_metal(
    -> ID MEDIUMINT NOT NULL AUTO_INCREMENT,
    -> metal_name VARCHAR(15),
    -> date DATE,
    -> data_source VARCHAR(5)
    -> soldBYN FLOAT,
    -> soldUSD FLOAT,
    -> buyBYN FLOAT,
    -> buyUSD FLOAT,
    -> PRIMARY KEY(ID)
    -> );
    '''

from nbrb import Nbrb
from bps import Bps
import datetime
import argparse
import MySQLdb

ENCODING_IN_MYSQL = 'SET NAMES utf8'

DB = MySQLdb.connect(host='127.0.0.1',
                     user='semion',
                     password='11111111',
                     db='metal_price'
                     )
cur = DB.cursor()


def valid_date(date):
    try:
        return datetime.datetime.strptime(date[:10], "%Y-%m-%d")
    except ValueError:
        msg = "Неправильная дата: '{0}'.".format(date)
        raise argparse.ArgumentTypeError(msg)


def insert_db(source, data_source):
    cur.execute(ENCODING_IN_MYSQL)
    result = source.get_prices()
    try:
        for metal in result:
            sql_insert_price = "INSERT INTO price_metal3(metal_name,date,data_source, soldBYN,soldUSD,buyBYN,buyUSD) VALUES (%s, %s, %s, %s, %s, %s,%s)"
            data = (metal.name.encode('utf-8').decode('latin-1'), metal.date, data_source, metal.price.soldBYN,
                                metal.price.soldUSD, metal.price.buyBYN, metal.price.buyUSD)

            cur.execute(sql_insert_price, data)
            DB.commit()
    except:
        DB.rollback()


if __name__ == '__main__':
    source_nbrb = Nbrb()
    source_bps = Bps()
    insert_db(source_nbrb, 'nbrb')
    insert_db(source_bps, 'bps')
