from bs4 import BeautifulSoup
from data_source import DataSource
from metal import Metal
from metal import Price
import requests
import datetime

BPS_URL = 'http://www.bps-sberbank.by/43257f17004e948d/dm_rates'
BPS_NUMBER_OF_PRICES = 4


class Bps(DataSource):
    def site_request(self, url):
        try:
            r = requests.get(url)
            return r
        except requests.ConnectionError:
            raise Exception(requests.ConnectionError('Нет соединения с интернетом'))
        except requests.HTTPError as http_error:
            raise Exception(requests.HTTPError(http_error))

    def get_prices(self):
        r = Bps.site_request(self, BPS_URL)
        soup = BeautifulSoup(r.text, 'html.parser')
        metals = soup.find_all("div", "curr_row")
        list_of_metals = [m.text for m in metals]
        date = datetime.datetime.now().strftime("%Y-%m-%d")
        result = []

        req = Bps.site_request(self, BPS_URL)
        soup_date = BeautifulSoup(req.text, 'html.parser')
        prices = soup_date.find_all("div", class_="td-text")
        price_of_metals = []
        for price in prices:
            price_of_metals.append(price.text)
        start = 0
        for metals in list_of_metals:
            price_of_metal = price_of_metals[start: start + BPS_NUMBER_OF_PRICES]
            start += BPS_NUMBER_OF_PRICES
            metal = Metal()
            metal.price = Price(soldBYN=price_of_metal[0].replace(",", "."), soldUSD=price_of_metal[1].replace(",", "."),
                                buyBYN=price_of_metal[2].replace(",", "."), buyUSD=price_of_metal[3].replace(",", "."))
            metal.name = metals
            metal.date = date
            result.append(metal)
        return result